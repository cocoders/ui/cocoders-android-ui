package org.c3.cocoders.ui

import androidx.compose.Composable
import androidx.ui.foundation.Text
import androidx.ui.material.Scaffold
import androidx.ui.tooling.preview.Preview

@Composable fun declareCoCodersUi() {
    Scaffold(bodyContent = {
        Text("Hello CoCoders")
    })
}

@Preview
@Composable
fun DefaultPreview() {
    declareCoCodersUi()
}
