package org.c3.cocoders.ui

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.ui.foundation.Text
import androidx.ui.material.MaterialTheme
import androidx.ui.material.Surface
import androidx.ui.test.ComposeTestRule
import androidx.ui.test.assertIsDisplayed
import androidx.ui.test.createComposeRule
import androidx.ui.test.findByText
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CoCodersUiTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUp() {
        composeTestRule.setContent {
            MaterialTheme { Surface { declareCoCodersUi() } }
        }
    }

    @Test
    fun when_app_launches_verify_app_name_is_displayed() {
        findByText("Hello CoCoders").assertIsDisplayed()
    }

}

fun ComposeTestRule.launchCoCodersApp() {
    setContent {
        //JetnewsStatus.resetState()
        MaterialTheme { Surface { declareCoCodersUi() } }
    }
}
